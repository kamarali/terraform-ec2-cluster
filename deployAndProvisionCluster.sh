#!/bin/bash

# todo: https://stackoverflow.com/questions/8403409/how-can-i-run-a-command-only-after-some-other-commands-have-run-successfully

echo "Generating public private key pair to communicate with EC2 instances"
ssh-keygen -N '' -f ec2_key_pair <<< n

echo "Running terraform"
cd ./terraform/
terraform init && terraform plan -out plan.tfplan && terraform apply plan.tfplan
mv ./inventory ./hosts ./../ansible/
mv ./zoo.cfg ./nifi.properties.nifi_1 ./nifi.properties.nifi_2 ./state-management.xml ./../ansible/playbooks/files
cd ..

echo "Waiting 1:30 mins for ec2 instances to deploy"
sleep 90
echo "Running Ansible"
cd ./ansible/
./save_hosts.sh && ansible-playbook playbooks/main.yml
cd ..