#!/bin/bash

# This script takes the hosts file and adds each one to the known_hosts file so we can ssh onto them with no issues from the calling machine.
for i in $(cat hosts)
do
  ssh-keyscan $i >> ~/.ssh/known_hosts
done
