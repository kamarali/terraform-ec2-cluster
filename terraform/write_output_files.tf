#############################################################################################
## Outputting the public DNS of the EC2 instances to a file which can be used with ansible ##
#############################################################################################

data "template_file" "ansible_inventory_template" {
  template = file("${path.module}/templates/inventory_template.cfg")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2,
    aws_instance.nifi_node_.0,
    aws_instance.nifi_node_.1
  ]
  vars = {
    zookeeper_1 = aws_instance.zookeeper_node_.0.public_dns
    zookeeper_2 = aws_instance.zookeeper_node_.1.public_dns
    zookeeper_3 = aws_instance.zookeeper_node_.2.public_dns
    nifi_1      = aws_instance.nifi_node_.0.public_dns
    nifi_2      = aws_instance.nifi_node_.1.public_dns
  }
}

resource "null_resource" "ansible_inventory_output" {
  triggers = {
    template_rendered = data.template_file.ansible_inventory_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.ansible_inventory_template.rendered}' > inventory"
  }
}

##################################
## Outputting the zookeeper IPS ##
##################################
data "template_file" "hosts_template" {
  template = file("${path.module}/templates/hosts_template")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2,
    aws_instance.nifi_node_.0,
    aws_instance.nifi_node_.1
  ]
  vars = {
    zookeeper_1 = aws_instance.zookeeper_node_.0.public_dns
    zookeeper_2 = aws_instance.zookeeper_node_.1.public_dns
    zookeeper_3 = aws_instance.zookeeper_node_.2.public_dns
    nifi_1      = aws_instance.nifi_node_.0.public_dns
    nifi_2      = aws_instance.nifi_node_.1.public_dns
  }
}

resource "null_resource" "hosts_output" {
  triggers = {
    template_rendered = data.template_file.hosts_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.hosts_template.rendered}' > hosts"
  }
}

###############################################################
## Outputting the private IPS of the Zookeeper EC2 instances ##
###############################################################

data "template_file" "zookeeper_cfg_template" {
  template = file("${path.module}/templates/zoo_cfg_template.cfg")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2,
  ]
  vars = {
    zookeeper_1_private_ip = aws_instance.zookeeper_node_.0.private_ip
    zookeeper_2_private_ip = aws_instance.zookeeper_node_.1.private_ip
    zookeeper_3_private_ip = aws_instance.zookeeper_node_.2.private_ip
  }
}

resource "null_resource" "zookeeper_cfg_output" {
  triggers = {
    template_rendered = data.template_file.zookeeper_cfg_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.zookeeper_cfg_template.rendered}' > zoo.cfg"
  }
}

##########################################################################
## Outputting the zookeeper IPS for nifi properties for both nifi boxes ##
##########################################################################

data "template_file" "nifi_1_properties_template" {
  template = file("${path.module}/templates/nifi.properties.template")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2,
    aws_instance.nifi_node_.0,
  ]
  vars = {
    zookeeper_1_private_ip = aws_instance.zookeeper_node_.0.private_ip
    zookeeper_2_private_ip = aws_instance.zookeeper_node_.1.private_ip
    zookeeper_3_private_ip = aws_instance.zookeeper_node_.2.private_ip
    nifi_node_private_ip   = aws_instance.nifi_node_.0.private_ip
  }
}

resource "null_resource" "nifi_1_properties_output" {
  triggers = {
    template_rendered = data.template_file.nifi_1_properties_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.nifi_1_properties_template.rendered}' > nifi.properties.nifi_1"
  }
}

data "template_file" "nifi_2_properties_template" {
  template = file("${path.module}/templates/nifi.properties.template")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2,
    aws_instance.nifi_node_.1,
  ]
  vars = {
    zookeeper_1_private_ip = aws_instance.zookeeper_node_.0.private_ip
    zookeeper_2_private_ip = aws_instance.zookeeper_node_.1.private_ip
    zookeeper_3_private_ip = aws_instance.zookeeper_node_.2.private_ip
    nifi_node_private_ip   = aws_instance.nifi_node_.1.private_ip
  }
}

resource "null_resource" "nifi_2_properties_output" {
  triggers = {
    template_rendered = data.template_file.nifi_2_properties_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.nifi_2_properties_template.rendered}' > nifi.properties.nifi_2"
  }
}

#####################################################################
## Outputting the zookeeper IPS for nifi state-management-template.xml file ##
#####################################################################
data "template_file" "nifi_state_management_template" {
  template = file("${path.module}/templates/state-management-template.xml")
  depends_on = [
    aws_instance.zookeeper_node_.0,
    aws_instance.zookeeper_node_.1,
    aws_instance.zookeeper_node_.2
  ]
  vars = {
    zookeeper_1_private_ip = aws_instance.zookeeper_node_.0.private_ip
    zookeeper_2_private_ip = aws_instance.zookeeper_node_.1.private_ip
    zookeeper_3_private_ip = aws_instance.zookeeper_node_.2.private_ip
  }
}

resource "null_resource" "nifi_state_management_output" {
  triggers = {
    template_rendered = data.template_file.nifi_state_management_template.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.nifi_state_management_template.rendered}' > state-management.xml"
  }
}