###############################################################################
## Creating security group for the EC2 instances ##
###############################################################################
resource "aws_security_group" "zookeeper_security_group" {
  name   = "zookeeper_security_group"
  vpc_id = aws_vpc.vpc.id

  // Outbound rules
  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Inbound rules
  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22

    // Uses the SSH_IP variable set in terraform.tfvars to make sure you can only ssh onto box from specified IP
    cidr_blocks = ["${var.SSH_IP}/32"]
  }

  ingress {
    from_port = 2181
    protocol  = "tcp"
    to_port   = 2181

    cidr_blocks = ["50.50.50.0/24"]
  }

  ingress {
    from_port = 2888
    protocol  = "tcp"
    to_port   = 2888

    cidr_blocks = ["50.50.50.0/24"]
  }

  ingress {
    from_port = 3888
    protocol  = "tcp"
    to_port   = 3888

    cidr_blocks = ["50.50.50.0/24"]
  }
}

resource "aws_security_group" "nifi_security_group" {
  name   = "nifi_security_group"
  vpc_id = aws_vpc.vpc.id

  // Outbound rules
  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Inbound rules
  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22

    // Uses the SSH_IP variable set in terraform.tfvars to make sure you can only ssh onto box from specified IP
    cidr_blocks = ["${var.SSH_IP}/32"]
  }

  ingress {
    from_port = 8080
    protocol  = "tcp"
    to_port   = 8080

    cidr_blocks = ["50.50.50.0/24", "${var.SSH_IP}/32"]
  }

  ingress {
    from_port = 9999
    protocol  = "tcp"
    to_port   = 9999

    cidr_blocks = ["50.50.50.0/24"]
  }

  ingress {
    from_port = 9998
    protocol  = "tcp"
    to_port   = 9998

    cidr_blocks = ["50.50.50.0/24"]
  }
}