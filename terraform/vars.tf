variable "AWS_REGION" {
  default = "eu-west-2"
}

variable "AMI" {
  default = "ami-0fc841be1f929d7d1"
}

variable "ZOOKEEPER_INSTANCE_SIZE" {
  default = "t2.micro"
}

variable "NIFI_INSTANCE_SIZE" {
  default = "t2.micro"
}

variable "SSH_IP" {}
variable "KEY_NAME" {}
variable "PUBLIC_KEY_PATH" {}