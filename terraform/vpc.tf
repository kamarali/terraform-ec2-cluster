###############################################################################
## Creating the VPC (Virtual Private Network) ##
###############################################################################

resource "aws_vpc" "vpc" {
  cidr_block           = "50.50.50.0/24"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"

  tags = {
    Name = "terraform-vpc"
  }
}

###############################################################################
## Creating the two public subnets ##
###############################################################################

// This subnet contains IPs 50.50.50.0 - 50.50.50.63
resource "aws_subnet" "subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "50.50.50.0/26"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}a"

  tags = {
    Name = "data-center-1"
  }
}

// This subnet contains IPs 50.50.50.64 - 50.50.50.127
resource "aws_subnet" "subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "50.50.50.64/26"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.AWS_REGION}b"

  tags = {
    Name = "data-center-2"
  }
}
