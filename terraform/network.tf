###############################################################################
## Creating internet gateway (allows VPC to connect to the internet) ##
###############################################################################

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
}

###############################################################################
## Creating route table for subnets to they can reach the internet ##
###############################################################################

resource "aws_route_table" "custom_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

// Associating the 2 subnets with the custom route table
resource "aws_route_table_association" "crt_subnet_1" {
  route_table_id = aws_route_table.custom_route_table.id
  subnet_id      = aws_subnet.subnet_1.id
}

resource "aws_route_table_association" "crt_subnet_2" {
  route_table_id = aws_route_table.custom_route_table.id
  subnet_id      = aws_subnet.subnet_2.id
}