## Introduction
This set of terraform scripts have been designed to create the following infrastructure on AWS:

![see ./infrastructure_diagram](infrastructure_diagram.png)

In particular they will spin up:

* A VPC in the specified region (50.50.50.0/24 -> 50.50.50.0 - 50.50.50.255)
* Two subnet's within this VPC (each within a different data center)
    * Subnet_1: 50.50.50.0/26 (50.50.50.0 - 50.50.50.63)
    * Subnet_2: 50.50.50.64/26 (50.50.50.64 - 50.50.50.127)
* 5 EC2 Red Hat Enterprise instances spread over both subnet's. All accessible by ssh from your specified IP (see 'Setup' section) 
and all have access to the internet. The public DNS for each box will be outputted at the end of the `terraform apply` command.
    * 3 in Subnet_1
    * 2 in Subnet_2
* After this is will output the public DNS's of each box into a file called hosts, ready for Ansible to provision the machines
## Setup
1. Set up your AWS credentials setup (skip this step if you already have aws credentials setup)
    * Easiest way to do this is to install AWS Cli:
    
        https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
    * Then use the `aws configure` command
    
        https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

2. Generate keys to connect to EC2 instances
    * The following docs have details on how to do this
        
        https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html
    * On linux/mac you can use the following command: (replacing 'ec2_key_pair' with whatever you want to call the key)
    
        `ssh-keygen -f ec2_key_pair`
        
        This created 2 files on my machine, `ec2_key_pair` and `ec2_key_pair.pub`

3. Install terraform (skip this step if you have already installed terraform)
    
    * https://learn.hashicorp.com/terraform/getting-started/install.html
    
4. Update the `terraform.tfvars` file with the required variables
    * Your public IP (use the link below if you don't know this).
    This will mean you can only ssh onto your EC2 instances from your home IP. 
    The configuration for this can be found in ec2.tf file in the aws_security_group section.
    If you really don't want to do this you can use `0.0.0.0/0`
    
        https://www.whatismyip.com/what-is-my-public-ip-address/
        
    * The name of the key you created in the previous step (step 2.)
    
    * The location of the public key
    
    For example the terraform.tfvars file might look like this:
   
   
    SSH_IP = "100.100.100.100"
    KEY_NAME = "ec2_key_pair"
    PUBLIC_KEY_PATH = "./ec2_key_pair.pub"
     

## Useful commands:

* Initialise terraform

    `terraform init`

* Plan terraform and output the plan so we can be sure this is exactly what will be applied:

    `terraform plan -out plan.tfplan`

* Apply planned terraform

    `terraform apply plan.tfplan`

* Destroy the terraformed infrastructure

    `terraform destroy` 