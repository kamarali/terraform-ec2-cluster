###############################################################################
## Linking the key pair which will be used to connect to the EC2 ##
###############################################################################
resource "aws_key_pair" "ec2_key_pair" {
  key_name   = var.KEY_NAME
  public_key = file(var.PUBLIC_KEY_PATH)
}

###############################################################################
## Creating the EC2 instances ##
###############################################################################

resource "aws_instance" "zookeeper_node_" {
  count = 3
  ami           = var.AMI
  instance_type = var.ZOOKEEPER_INSTANCE_SIZE

  subnet_id              = count.index % 2 == 0 ? aws_subnet.subnet_1.id : aws_subnet.subnet_2.id
  vpc_security_group_ids = [aws_security_group.zookeeper_security_group.id]

  key_name = aws_key_pair.ec2_key_pair.id

  tags = {
    Name = "zookeeper_${count.index}"
  }
}

resource "aws_instance" "nifi_node_" {
  count = 2
  ami           = var.AMI
  instance_type = var.NIFI_INSTANCE_SIZE

  subnet_id              = count.index % 2 == 0 ? aws_subnet.subnet_1.id : aws_subnet.subnet_2.id
  vpc_security_group_ids = [aws_security_group.nifi_security_group.id]

  key_name = aws_key_pair.ec2_key_pair.id

  tags = {
    Name = "nifi_${count.index}"
  }
}
