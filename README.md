## Introduction

This project first deploys infrastructure on AWS using terraform, and after this we provision these created boxes using Ansible.

## Pre Reqs

1. Terraform installed
    * https://learn.hashicorp.com/terraform/getting-started/install.html
2. Ansible installed 
    * https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
3. Go to `./terraform/terraform.tfvars` and populate `SSH_IP` with you public IP address. This will allow ssh from only your public IP address.
I'd also recommend adding `NIFI_INSTANCE_SIZE = "t2.small"` as the t2.micro boxes aren't powerful enough for nifi (please mind this will cost you!)
    * https://whatismyipaddress.com
4. Set up aws credentials
    * Easiest way to do this is to install AWS Cli:
        
      https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
    * Then use the `aws configure` command
        
      https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
    
## Running scripts
To run the project, deploy the infrastructure and provision it with 3 zookeeper nodes run the following script:

`deployAndProvisionCluster.sh`

This will first run the terraform scripts, taking the files which are written out `hosts` and `zoo.cfg` and move them over into the ansible directory.
Following this it will wait 90 seconds to allow the instances enough time to spin up before running the installZookeeper playbook, specifically targetting 
three of the provisioned machines which will be used to host zookeeper.


## Destroying infrastructure 
To destroy the provisioned infrastructure simply run:

`./destroyCluster.sh`